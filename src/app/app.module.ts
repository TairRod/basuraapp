import { RutaPage } from './../pages/ruta/ruta';
import { BuscandoPage } from './../pages/buscando/buscando';
import { PagosPage } from './../pages/pagos/pagos';
import { ConfiguracionPage } from './../pages/configuracion/configuracion';
import { HistorialPage } from './../pages/historial/historial';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {Geolocation} from '@ionic-native/geolocation'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    HistorialPage,
    ConfiguracionPage,
    PagosPage,
    BuscandoPage,
    RutaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    HistorialPage,
    ConfiguracionPage,
    PagosPage,
    BuscandoPage,
    RutaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
