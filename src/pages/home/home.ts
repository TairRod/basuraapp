import { RutaPage } from './../ruta/ruta';
import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import { AlertController } from 'ionic-angular';


declare var google:any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  @ViewChild('map') mapRef:ElementRef;
  map:any;
  lat:any;
  lng:any;
  ubicacionactual:any;
  ubicacion: string;
  testCheckboxOpen: boolean;
  testCheckboxResult;
  constructor(public navCtrl: NavController, private geo: Geolocation, public loadingCtrl: LoadingController, public zone:NgZone, public alertCtrl: AlertController) {

  }

  ionViewDidLoad(){
    this.geo.getCurrentPosition().then((resp) => {
      this.lat=resp.coords.latitude;
      this.lng=resp.coords.longitude;
      this.showMap();
     }).catch((error) => {
       console.log('Error getting location', error);
     });
    
  }

  showMap(){
    const location = new google.maps.LatLng(this.lat, this.lng);
    this.getAdress();
    const options={
      center:location,
      zoom:15,
      streetViewControl:false,
      mapTypeId:'roadmap',
      zoomControl: false,
      mapTypeControl:false,
      fullscreenControl:false
    };

    this.map = new google.maps.Map(this.mapRef.nativeElement, options);

    var marker = new google.maps.Marker({
      position: location,
      map:this.map,
      draggable:true,

    });
    
    google.maps.event.addListener(marker, 'dragend',(marker)=>{
      var latLng = marker.latLng; 
      this.lat = latLng.lat();
      this.lng = latLng.lng();
    this.getAdress()
   }); 
   
  }

  getAdress(){
    let geocoder= new google.maps.Geocoder;
    let latlng = {lat: this.lat, lng: this.lng};
    geocoder.geocode({'location': latlng}, (results, status) => {
      this.zone.run (() => { 
        this.ubicacion=results[0].formatted_address;
      });
     
   });
  }

  Ruta(){
    this.navCtrl.push(RutaPage);
  }

  
  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Buscando recolector',
      duration: 5000
    });
  
    loading.onDidDismiss(() => {
     this.Ruta();

    });
  
    loading.present();
  }
  showRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Método de pago para esta recolección');

    alert.addInput({
      type: 'radio',
      label: 'Efectivo',
      value: 'efectivo',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Tarjeta',
      value: 'tarjeta',
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log('Checkbox data:', data);
        this.testCheckboxOpen = false;
        this.testCheckboxResult = data;
        this.presentLoadingCustom();
      }
    });
    alert.present();
  }

}
