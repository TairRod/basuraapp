import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscandoPage } from './buscando';

@NgModule({
  declarations: [
    BuscandoPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscandoPage),
  ],
})
export class BuscandoPageModule {}
